﻿#include <iostream>
#include <omp.h>
#include <stdio.h>
#include <ctime>
#include <algorithm>

void RandomArrayGen(double*& arr, int size, int max)
{
	for (int i = 0; i < size; i++)
	{
		arr[i] = rand() % max;
	}
	std::cout << "Generated\n";
}



/// <summary>
/// Path to file is : C:\Program Files (x86)\Microsoft SDKs\MPI\MPIFirst\Release\ . Then exec "mpiexec -n $Processes_Count$ MPIFirst.exe, This will ask you to enter array size. choose over 10000, don't be shy.
/// </summary>
/// <param name="arr"></param>
/// <param name="procNumElems"></param>
/// <param name="procStartInd"></param>
/// <param name="Size"></param>
void InitProc(double*& arr, int*& result, int& Size, double*& sortedResult) {

	do {
		printf("\nEnter the size of the array: ");
		std::cin >> Size;
		if (Size < 0) {
			printf("Size of the array must be >= than number of processes! \n");
		}
	} while (Size < 0);

	result = new int[Size]; //grep
	sortedResult = new double[Size];

	arr = new double[Size];

	RandomArrayGen(arr, Size, 100);
}



void RankCalc(double* arr, int Size, int* pRanks)
{
	for (int i = 0; i < Size; i++)
	{
		pRanks[i] = 0;
	}

#pragma omp parallel for num_threads(4)
	for (int i = 0; i < Size; i++) {

		for (int k = 0; k < Size; k++)
		{
			if (arr[k] > arr[i] || (arr[k] == arr[i] && k > i))
				pRanks[k]++;
		}
	}
}


void TerminateProcess(double*& arr, int*& resultRanks, int& Size, double*& result)
{
	delete[] arr;
	delete[] resultRanks;
	delete[] result;
}

void PrintArray(double* arr, int& size)
{
	for (int i = 0; i < size; i++)
	{
		printf("%f; ", arr[i]);
	}
}

int main(int argc, char* argv[])
{
	double* array;
	int* resultRanks;
	int size;
	double* sortedResult;
	//Initialise array
	InitProc(array, resultRanks, size, sortedResult);

	std::clock_t start;
	double duration;

	start = std::clock();
	RankCalc(array, size, resultRanks);
	for (int i = 0; i < size; i++)
	{
		sortedResult[resultRanks[i]] = array[i];
	}
	duration = (std::clock() - start) / CLOCKS_PER_SEC;

	printf("Time: %f; \n", duration);

	TerminateProcess(array, resultRanks, size, sortedResult);

	return 0;
}