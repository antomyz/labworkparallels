﻿#include "mpi.h"

#include <stdio.h>
#include <iostream>


int rank;
int num;

/// <summary>
/// Path to file is : C:\Program Files (x86)\Microsoft SDKs\MPI\MPIFirst\Release\ . Then exec "mpiexec -n $Processes_Count$ MPIFirst.exe, This will ask you to enter array size. choose over 10000, don't be shy.
/// </summary>
/// <param name="arr"></param>
/// <param name="procNumElems"></param>
/// <param name="procStartInd"></param>
/// <param name="Size"></param>

void DataSorting(double* arr, int* procNumElems, int* procStartInd, int Size) {
	//sndnum - кількість елементів, надісланих процесу
	//pSendInd - індекс першого елементу, надісланого процесу
	int* sndnum = new int[num];
	int* sndIndex = new int[num];


	int UnallocElem = Size;
	int UnallProc = num;

	// Визначаю розміщення рядків матриць для поточного процесу

	for (int i = 0; i < num; i++)
	{
		sndnum[i] = UnallocElem / UnallProc;
		sndIndex[i] = Size - UnallocElem;
		UnallocElem -= sndnum[i];
		UnallProc -= 1;
	}
	MPI_Scatter(sndnum, 1, MPI_INT, procNumElems, 1, MPI_INT, 0, MPI_COMM_WORLD);
	MPI_Scatter(sndIndex, 1, MPI_INT, procStartInd, 1, MPI_INT, 0, MPI_COMM_WORLD);
}
void RandomArrayGen(double*& arr, int size, int max)
{
	for (int i = 0; i < size; i++)
	{
		arr[i] = rand() % max;
	}
	std::cout << "Successfully generated Random Array\n";
}

void InitProcess(double*& array, int*& resultRanks, int& size, double*& sortedResult, int*& pNumberRanks, int& pStartInd, int& pNumElems) {
	if (rank == 0)
	{
		do {
			printf("\nEnter the size of the array: ");
			std::cin >> size;
			if (size < num) {
				printf("Size of the array must be >= than number of processes! \n");
			}
		} while (size < num);
	}
	MPI_Bcast(&size, 1, MPI_INT, 0, MPI_COMM_WORLD);

	resultRanks = new int[size];
	sortedResult = new double[size];

	array = new double[size];

	if (rank == 0) {
		RandomArrayGen(array, size, 100);
	}

	MPI_Bcast(array, size, MPI_DOUBLE, 0, MPI_COMM_WORLD);

	DataSorting(array, &pNumElems, &pStartInd, size);

	pNumberRanks = new int[pNumElems];
}




void TestDistribution(double* arr, int Size, int pNumElems, int pStartInd) {
	if (rank == 0) {
		printf("Array: \n");
		for (int i = 0; i < Size; i++)
		{
			printf("%f; ", arr[i]);
		}
	}
	fflush(stdout);
	MPI_Barrier(MPI_COMM_WORLD);
	for (int i = 0; i < num; i++)
	{
		if (rank == i) {
			printf("\nProcRank = %d, Start Index = %d, Num = %d \n", rank, pStartInd, pNumElems);
			for (int k = pStartInd; k < pNumElems + pStartInd; k++)
			{
				printf("%f; ", arr[k]);
			}
		}
		fflush(stdout);
		MPI_Barrier(MPI_COMM_WORLD);
	}
}



void GatherResults(int* pProcResult, int* result, int Size) {
	int* receiveNum = new int[num];
	int* receiveInd = new int[num];
	int RestParts = Size;
	receiveInd[0] = 0;
	receiveNum[0] = Size / num;
	for (int i = 1; i < num; i++) {
		RestParts -= receiveNum[i - 1];
		receiveNum[i] = RestParts / (num - i);
		receiveInd[i] = receiveInd[i - 1] + receiveNum[i - 1];
	}

	MPI_Allgatherv(pProcResult, receiveNum[rank], MPI_INT, result,
		receiveNum, receiveInd, MPI_INT, MPI_COMM_WORLD);

	delete[] receiveNum;
	delete[] receiveInd;
}

void TerminateProcess(double*& arr, int*& resultRanks, int& Size, double*& result, int*& pNumberRanks)
{
	delete[] arr;
	delete[] resultRanks;
	delete[] result;
	delete[] pNumberRanks;
}

void PrintArray(double* arr, int& size)
{
	printf("%d; ", 1);
	for (int i = 0; i < size; i++)
	{
		printf("%f; ", arr[i]);
	}
}
void CalcArrayRanks(double* arr, int Size, int pNumElems, int pStartInd, int* pRanks)
{
	for (int i = 0; i < pNumElems; i++)
	{
		pRanks[i] = 0;
	}

	for (int i = 0; i < Size; i++) {
		for (int k = pStartInd; k < pNumElems + pStartInd; k++)
		{
			if (arr[k] > arr[i] || (arr[k] == arr[i] && k > i))
				pRanks[k - pStartInd]++;
		}
	}
}//greg

int main(int argc, char* argv[])
{


	MPI_Init(&argc, &argv);

	MPI_Comm_size(MPI_COMM_WORLD, &num);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);

	if (rank == 0)
		printf("%d Processes\n", num);

	double* array;
	int* resultRanks;
	int size;
	int* pNumberRanks;
	double* sortedResult;
	int pNumElems;
	int pStartInd;

	//Initialise and distribute

	///
	if (rank == 0)
	{
		do {
			printf("\nEnter the size of the array: ");
			std::cin >> size;
			if (size < num) {
				printf("Size of the array must be >= than number of processes! \n");
			}

		} while (size < num);
	}

	MPI_Bcast(&size, 1, MPI_INT, 0, MPI_COMM_WORLD);

	resultRanks = new int[size];
	sortedResult = new double[size];

	array = new double[size];

	if (rank == 0) {
		RandomArrayGen(array, size, 100);
	}

	MPI_Barrier(MPI_COMM_WORLD);
	auto start = MPI_Wtime();

	MPI_Bcast(array, size, MPI_DOUBLE, 0, MPI_COMM_WORLD);

	DataSorting(array, &pNumElems, &pStartInd, size);

	pNumberRanks = new int[pNumElems];
	///

	pNumberRanks = new int[pNumElems];



	CalcArrayRanks(array, size, pNumElems, pStartInd, pNumberRanks);

	GatherResults(pNumberRanks, resultRanks, size);

	if (rank == 0)
	{
		for (int i = 0; i < size; i++)
		{
			sortedResult[resultRanks[i]] = array[i];
		}
	}

	MPI_Barrier(MPI_COMM_WORLD);
	auto end = MPI_Wtime();

	if (rank == 0)
	{
		printf("Time: %f \n", end - start);

			//PrintArray(sortedResult, size);
		
	}

	TerminateProcess(array, resultRanks, size, sortedResult, pNumberRanks);


	MPI_Finalize();


	return 0;
}